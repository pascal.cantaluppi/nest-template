import { Status } from '../task-status.enum';
import { IsOptional, IsIn, IsNotEmpty } from 'class-validator';
export class GetTasksFilteredDto {
  @IsOptional()
  @IsIn([Status.OPEN, Status.IN_PROGRESS, Status.DONE])
  status: Status;
  @IsOptional()
  @IsNotEmpty()
  search: string;
}
