# build environment
FROM node:14.15.0-alpine as builder
WORKDIR /app
COPY . .
RUN npm i
RUN npm run prebuild
RUN npm run build

# production environment
FROM node:14.15.0-alpine 
WORKDIR /app
COPY --from=builder /app/package.json .
COPY --from=builder /app/dist ./dist
COPY --from=builder /app/node_modules ./node_modules
CMD ["npm", "run", "start:prod"]
