<p>
<a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

<p>A progressive Node.js framework for building efficient and scalable server-side applications.</p>

## Description

NestJS Framework TypeScript Project Template.


## Run the API

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```
